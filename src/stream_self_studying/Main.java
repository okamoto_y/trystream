package stream_self_studying;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

/**
 * メインクラス
 * @author okamoto_y
 */
public class Main {

	/**
	 * メイン処理
	 * @param args コンソール入力
	 */
	public static void main(String[] args) {

		System.out.println("main");
		System.out.println("==");

//		IntermediateOperation.tryFillter();
//		IntermediateOperation.tryMap();
//		IntermediateOperation.trySorted();
//		IntermediateOperation.trySkipLimit();
//		IntermediateOperation.tryPeek();
//		IntermediateOperation.tryDistinct();

//		TerminalOperation.tryFindFirst();
//		TerminalOperation.tryXxxMatch();
//		TerminalOperation.tryToArray();
//		TerminalOperation.tryCollect();
//		TerminalOperation.tryMinMax();
//		TerminalOperation.tryCount();
		TerminalOperation.tryReduce();

//		VariousStreams.tryVariousStreams();
//		letsStream();
	}

	/**
	 * レッツストリーム
	 */
	public static void letsStream() {

		List<String> prefectures = Arrays.asList(
				"Hokkaido", "Tokyo", "Kyoto", "Osaka", "Hukuoka", "Okinawa", "Hyogo", "Kanagawa", "Gihu", "");

		prefectures.stream()
				// Predicate→引数1つ、戻り値Boolean
				// ラムダ式
				.filter(s -> s.length() >= 5)
				// メソッド参照
//				.filter(String::isEmpty)

				// Function→引数1つ、戻り値R
				// 匿名クラス
//				.map(new Function<String, String>() {
//					@Override
//					public String apply(String s) {
//						return s.toUpperCase();
//					}
//				})
//				// ラムダ式1
//				.map(s -> {
//					return s.toUpperCase();
//				})
//				// ラムダ式2
//				.map(s -> s.toUpperCase())
				// メソッド参照
				.map(String::toUpperCase)

				// Consumer→引数1つ、戻り値無し
				// ラムダ式
//				.forEach(s -> System.out.println(s));
				// メソッド参照
				.forEach(System.out::println);

//		BiFunction<Integer, String, String> bi = (i, s) -> i + s;
		System.out.println(tryArgumentLambda((i, s) -> i + s));
	}

	public static String tryArgumentLambda (BiFunction<Integer, String, String> bi) {
		return bi.apply(100, "200");
	}

}
