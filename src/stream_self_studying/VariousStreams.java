package stream_self_studying;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * クラスの説明
 * @author okamoto_y
 */
public class VariousStreams {

	public static void tryVariousStreams() {

		Stream<String> fruits = Stream.of("Apple", "Banana", "Grape", "Orange");
		fruits.forEach(System.out::println);

		List<String> prefectures = new ArrayList<>(Arrays.asList("Osaka", "Hyogo", "Kyoto", "Nara"));
		prefectures.stream().forEach(System.out::println);

		IntStream.rangeClosed(1, 5).forEach(System.out::println);

		DoubleStream.concat(DoubleStream.of(1.1, 1.2), DoubleStream.of(2.1, 2.2)).forEach(System.out::println);
	}
}
