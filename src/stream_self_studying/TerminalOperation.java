package stream_self_studying;

import static java.lang.System.out;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TerminalOperation {

	private static List<String> wordList = Arrays.asList(
			"Apple", "Tanaka", "Mouse", "PC", "Monitor", "Azure", "Keyboard", "NieR", "Browser", "Detroit",
			"MachineLifeform", "Eve", "Fortnite", "Hokaben", "SuperMarioBro", "Apple", "Tanaka", "Android");

	public static void tryForEach() {
		// さんざん使ってるので省略
	}

	public static void tryFindFirst() {

		out.println("FindFirst\n==");
		out.println(wordList.stream()
				.filter(s -> s.startsWith("A"))
				.findFirst()
				.orElse("empty"));

		out.println(wordList.stream()
				.filter(s -> s.startsWith("X"))
				.findFirst());
	}

	public static void tryXxxMatch() {

		out.println("XxxMatch\n==");
		out.println(wordList.stream()
				.anyMatch(s -> s.contains("a")));
		out.println(wordList.stream()
				.allMatch(s -> s.length() > 3));
		out.println(wordList.stream()
				.noneMatch(s -> s.equals("")));
	}

	public static void tryToArray() {

		out.println("ToArray\n==");
		Arrays.asList(
				wordList.stream()
						.filter(s -> s.endsWith("e"))
						.sorted(Comparator.naturalOrder())
						.toArray())
				.stream()
				.forEach(out::println);

	}

	public static void tryCollect() {

		out.println("Collect\n==");
		List<String> ss = Stream.of("Tokyo", "Osaka", "Kyoto")
				.filter(s -> s.startsWith("T"))
				.collect(Collectors.toList());
		ss.stream().forEach(out::println);
	}

	public static void tryMinMax() {

		out.println("MinMax\n==");
		out.println(Stream.of(1, 4, 7)
				.min((i1, i2) -> i1 - i2).orElse(-1));
		out.println(Stream.of(1, 4, 7)
				.min((i1, i2) -> i2 - i1).orElse(-1));
		out.println(IntStream.of(2, 3, 5)
				.max().orElse(-1));
		out.println(IntStream.rangeClosed(4, 9)
				.max().orElse(-1));

	}

	public static void tryCount() {

		out.println("Count\n==");
		out.println(wordList.stream()
				.count()
				);
	}

	public static void tryReduce() {

		out.println("Reduce\n==");
		out.println(wordList.stream()
				.sorted()
				.reduce((result, str) -> result + "," + str)
				.orElse(""));

	}
}
