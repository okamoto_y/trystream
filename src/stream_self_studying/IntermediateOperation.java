package stream_self_studying;

import static java.lang.System.out;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * クラスの説明
 * @author okamoto_y
 */
public class IntermediateOperation {

	private static List<String> wordList = Arrays.asList(
			"Apple", "Tanaka", "Mouse", "PC", "Monitor", "Azure", "Keyboard", "NieR", "Browser", "Detroit",
			"MachineLifeform", "Eve", "Fortnite", "Hokaben", "SuperMarioBro", "Apple", "Tanaka");

	public static void tryFillter() {

		out.println("filter");
		out.println("==");
		wordList.stream()
				.filter(s -> s.startsWith("A"))
				.forEach(out::println);

		out.println("filter");
		out.println("==");
		wordList.stream()
				.filter(s -> s.contains("a"))
				.forEach(out::println);
	}

	public static void tryMap() {

		out.println("map");
		out.println("==");
		wordList.stream()
				.map(s -> s.length())
				.forEach(out::println);

		out.println("==");
		wordList.stream()
				.map(s -> s.toUpperCase())
				.forEach(out::println);
	}

	public static void trySorted() {

		out.println("sorted");
		out.println("==");
		wordList.stream()
				.sorted()
				.forEach(out::println);

		out.println("==");
		wordList.stream()
				.sorted(Comparator.reverseOrder())
				.forEach(out::println);

		out.println("==");
		wordList.stream()
				.sorted(Comparator.naturalOrder())
				.sorted((s1, s2) -> s1.length() - s2.length())
				.forEach(out::println);
	}

	public static void trySkipLimit() {

		out.println("skiplimit");
		out.println("==");
		wordList.stream()
				.skip(3L)
				.limit(5L)
				.forEach(out::println);
	}

	public static void tryPeek() {

		out.println("peek");
		out.println("==");
		wordList.stream()
			.peek(out::println)
			.map(s -> s.substring(2))
			.peek(s -> out.println("↓"))
			.peek(out::println)
			.filter(s -> s.length() > 5)
			.forEach(s -> out.println("6文字以上"));
	}

	public static void tryDistinct() {

		out.println("distinct");
		out.println("==");
		wordList.stream()
			.distinct()
			.forEach(With.B(1, (s, i) -> out.println(i + ":" + s)));
	}
}
